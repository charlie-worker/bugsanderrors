#Имеется разметка xgtf 
#(что такое и как что пишется объясню по запросу).
#Необходимо оставить в ней только те треки,
#которые живут дольше n секунд
#Ну а здесь тебе по факту надо удалить обькеты ,
#которые существуют менее n секунд.


import os
from sys import argv
import argparse
import xml.etree.cElementTree as ET
from xml.dom import minidom

parser = argparse.ArgumentParser(description='Killing low time tracks')
parser.add_argument('workingDir', type=str, help='Dir with .xgtf')
parser.add_argument('long', type=int, help='If object live less than n sek it will be killed')
args = parser.parse_args()

def checkXGTFs(dir):
    supportExts = ('.xgtf')
    for name in os.listdir(dir):
        path = os.path.join(dir, name)
        if os.path.isdir(path):
            checkXGTFs(path)
        elif name.lower().endswith(supportExts):
            tree = ET.ElementTree(file=path)
            root = tree.getroot()
            now = Killer(tree, path)

class Killer(object):

    def __init__(self, tree, path):
        nameOfData = '{http://lamp.cfar.umd.edu/viperdata#}'
        self.root = tree.getroot()
        self.tree = tree
        self.path = path
        self.framerate = float(self.root.find('.//' + nameOfData +
                                'fvalue').attrib.get('value'))
        self.checkTrack(self.root)
        self.tree.write(self.path)
# I now know about root.findall('object'), but i so close
# to end it, so gonna use old way

    def checkTrack(self, root):
        nameStart = '{http://lamp.cfar.umd.edu/viper#}'
        n = len(list(root))
        for i in range(n):
            if root[-i].tag == nameStart + 'object':
                print(list(root))
                framespan = root[-i].attrib.get('framespan')
                frames = convert(framespan)
                if frames * self.framerate < args.long:
                    print(frames)
                    root.remove(root[-i])
                    print(list(root))
                    print(i)
                    continue
#цикл ломается если находит потенциальный объект дял удаления,
#объект удаляется и на этот цикл прерывается, назависимо от того 
#как он итерируется
#18.03.2020
            else:
                self.checkTrack(root[-i])


def convert(distanse):
    start, stop = map(int, distanse.split(':'))
    return(stop-start)


checkXGTFs(args.workingDir)
